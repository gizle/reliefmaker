
module Reliefs

export reliefify_heightmap,  make_2d_grid_orto, make_zbuffer, mesh_bonding_box, read_model, perspective_to_orto,
    trans, scale, rotx, roty, rotz, hom, find_model_midpoint, transform,
    write_model
       

#include("types.jl")
#include("modelio.jl")
#include("bitmapify.jl")
include("relief.jl")
include("transforms.jl")
include("reliefmodelgen.jl")
include("zbuffer.jl")
end
