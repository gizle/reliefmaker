
using LinearAlgebra
using GeometryBasics

function trans(vec::Array{F}) where F
    mat = Matrix{F}(I,4,4)
    mat[1, 4] = vec[1]
    mat[2, 4] = vec[2]
    mat[3, 4] = vec[3]
    mat
end

function scale(vec::Array{F}) where F
    mat = Matrix{F}(I,4,4)
    mat[1,1] = vec[1]
    mat[2,2] = vec[2]
    mat[3,3] = vec[3]
    mat
end

function rotz(theta::F) where F
    mat = Matrix{F}(I,4,4)
    mat[1,1] =  cos(theta)
    mat[1,2] = -sin(theta)
    mat[2,1] =  sin(theta)
    mat[2,2] =  cos(theta)
    mat
end

function rotx(theta::F) where F
    mat = Matrix{F}(I,4,4)
    mat[2,2] =  cos(theta)
    mat[2,3] = -sin(theta)
    mat[3,2] =  sin(theta)
    mat[3,3] =  cos(theta)
    mat
end

function roty(theta::F) where F
    mat = Matrix{F}(I,4,4)
    mat[1,1] =  cos(theta)
    mat[1,3] =  sin(theta)
    mat[3,1] = -sin(theta)
    mat[3,3] =  cos(theta)
    mat
end

function hom(vec::Array{F}) where F
   [vec ; [1.0]]
end

function deg2rad(deg::F) where F
    (deg * pi) / 360.0
end

function rad2deg(rad::F) where F
    (rad * 360.0) / pi
end

function transform(mesh::AbstractMesh, m::Array{Float32,2})
    p1 = Vector{Float32}([0.0, 0.0, 0.0, 1.0])
    points = decompose(Point3f, mesh)
    faces = decompose(GLTriangleFace, mesh)
    normals = decompose_normals(mesh)

    newpoints = Vector{Point{3, Float32}}()
    n_points = length(points)
    for i = 1:n_points
        p1[1:3] = points[i][1:3]
        p1[4] = 1.0
        p1 = m*p1
        push!(newpoints, Point{3, Float32}(p1[1:3]))
    end
    Mesh(meta(newpoints; normals=copy(normals)), copy(faces))
end 

function perspective_to_orto(mesh::AbstractMesh, dist::Float32)
    points = decompose(Point3f, mesh)
    faces = decompose(GLTriangleFace, mesh)
    normals = decompose_normals(mesh)
    p1 = Vector{Float32}([0.0, 0.0, 0.0])
    
    newpoints = Vector{Point{3, Float32}}()
    n_points = length(points)
  
    for i = 1:n_points
        p1[1:3] = points[i][1:3]
        p1 = perspective_to_orto(p1, dist)
        push!(newpoints, Point{3, Float32}(p1))       
    end
    Mesh(meta(newpoints; normals=copy(normals)), copy(faces))
end

function perspective_to_orto(point::Vector{Float32}, dist::Float32)
    d = distance([0,0,-dist], point)
    #d = (-dist-point[3])
    z0 = d + dist
    x1 = point[1] * ( z0 / dist )
    y1 = point[2] * ( z0 / dist )
    [x1, y1, point[3]]
end

distance(p1, p2) = sqrt((p1[1] - p2[1])^2 + (p1[2] - p2[2])^2 + (p1[3] - p2[3])^2 )

