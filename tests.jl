
using Images, Colors, FixedPointNumbers
using HDF5

include("modelio.jl")
include("bitmapify.jl")
include("reliefify.jl")
include("transforms.jl")

function test_intersection()
    epsilon = 0.000001
    tri1 = Triangle([1.0, 1.0, 0.0], [1.0, -1.0, 0.0], [0.0, -1.0, 0.0],[0.0, 0.0, -1.0])
    ray1 = Ray([0.0, 0.0, -1.0], [0.0, 0.0, 1.0])
    tuv = triangle_intersect(tri1, ray1)
    println(tuv)
end

function test_read_model()
    read_model("/home/gisle/3dmodels/testmodels/dino.stl", true)
end

function test_model()
    tri1 = Triangle([-1.0, -1.0, 0.0],
                    [ 1.0, -1.0, 0.0],
                    [ 0.0,  1.0, 0.0],
                    [ 0.0,  0.0, 1.0])
    tri2 = Triangle([-1.0, -1.0, 1.0],
                    [ 1.0, -1.0, 1.0],
                    [ 0.0,  1.0, 0.0],
                    [ 0.0,  0.0, 1.0])
    arr = [tri1, tri2]
    Solid(arr)
end

function test_2d_mesh(model, dim)
    m = rotx(1.4) * rotz(-pi/8.0)
    model = transform_solid(model, m)
    #arr = make_2d_grid_orto(model, dim)
    #arr = reliefify_heightmap(arr)
    #arr
end

function make_relief(heightmap, pngname, alpha, theta)
  slopes = reliefify_heightmap(heightmap, 200.0, 75.0, 100.0, alpha, theta)
  slopes[slopes .< 0.0] = 0.0
  slopes /= maximum(slopes)
  save(pngname, grayim(slopes))
end