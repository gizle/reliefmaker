
using Images #, Colors, FixedPointNumbers
import JSON
using FileIO
using MeshIO

include("Reliefs.jl")

function read_json_file(path)
    open(path) do f
        content = read(f, String)
        JSON.parse(content)
    end
end

function make_relief(heightmap, xrange, yrange, alpha, theta)
    slopes, mask = Reliefs.reliefify_heightmap(Matrix{Float64}(heightmap), xrange, yrange, alpha, theta)
    #slopes[slopes .< 0.0] .= 0.0
    return slopes, mask
end

function make_bitmap(settings)
    dim = settings["dimension"]
    orig_mesh = FileIO.load(settings["infile"])

    maxpt, minpt = Reliefs.mesh_bounding_box(orig_mesh)
    #move to midpoint
    midpoint_trans = Reliefs.trans([(maxpt[1] + minpt[1]) / 2,
                                    (maxpt[2] + minpt[2]) / 2,
                                    (maxpt[3] + minpt[3]) / 2])

    #do rotations
    xrot = deg2rad(settings["xrotdeg"])
    yrot = deg2rad(settings["yrotdeg"])
    zrot = deg2rad(settings["zrotdeg"])
    m = Reliefs.rotx(xrot) * Reliefs.roty(yrot) * Reliefs.rotz(zrot) * Reliefs.trans(-midpoint_trans)
    mesh = Reliefs.transform(orig_mesh, Array{Float32}(m))

    println(Reliefs.mesh_bounding_box(mesh))
    #Transform to ortographic (does not work yet)
    if settings["projection"] == "perspective"
        mesh = Reliefs.perspective_to_orto(mesh, Float32(settings["distance"]))
    end

    #find dimensions (again)
    maxpt, minpt = Reliefs.mesh_bounding_box(mesh)

    xlen = maxpt[1] - minpt[1]
    ylen = maxpt[2] - minpt[2]
    zlen = maxpt[3] - minpt[3]
    aspect = xlen / ylen

    ydim = settings["dimension"]
    xdim = ydim * aspect
    
    scale = ydim / (ylen * 1.1)
    trans1 = Reliefs.trans([-minpt[1]+xlen*0.05 , -minpt[2] + ylen*0.05, -minpt[3]])
    smat = Reliefs.scale([scale, scale, 1.0])
    gridsize_mesh = Reliefs.transform(mesh, Array{Float32}(smat*trans1))

    dims = [Int(round(xdim)), Int(round(ydim))] 
    Reliefs.make_zbuffer(gridsize_mesh, dims), xlen, ylen, zlen
end


function scale_model(model, relief, xrange, yrange)
    xdim, ydim = size(relief)
    transmat = Reliefs.scale([xrange/xdim, yrange/ydim, 1.0])
    Reliefs.transform(model,  Array{Float32}(transmat))
end

function save_as_dat(path, relief)
    open(path, "w") do f
        write(f, "#written by reliefmaker\n")
        for i = 1:size(relief,1)
            for j = 1:size(relief,2)
                v = relief[i,j] * 100
                write(f, "$v ")
            end
            write(f,"\n")
        end
    end
end

function make_relief_from_bitmap(settings, heightmap, xrange, yrange, zrange)
    alpha = settings["alpha"]
    theta = settings["theta"]
    relief, mask = make_relief(heightmap, xrange, yrange, Float64(alpha), Float64(theta))
    outpath = settings["outfile"]

    if size(split(outpath, r".png$"))[1] > 1
        relief = (relief .- minimum(relief)) ./ (maximum(relief) .- minimum(relief))
        save(outpath, Gray.(map(clamp01nan, relief')))
    elseif size(split(outpath, r".stl$"))[1] > 1
        model = Reliefs.generate_model(relief, mask)
        model = scale_model(model, relief, xrange, yrange)
        f = File{format"STL_BINARY"}(outpath)
        save(f, model)
    else
        save_as_dat(outpath, relief)
    end
end

function make_relief_from_json(path)
    settings = read_json_file(path)
    heightmap, xrange, yrange, zrange = make_bitmap(settings)
    make_relief_from_bitmap(settings, heightmap, xrange, yrange, zrange)
end

if length(ARGS) > 0
    make_relief_from_json(ARGS[1])
end


