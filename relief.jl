


using SparseArrays
using DataStructures

function make_gradients(heightmap::Array{Float64,2}, xrange::Float64, yrange::Float64)
    xdim, ydim = size(heightmap)
    xstep = xrange / Float64(xdim)
    ystep = yrange / Float64(ydim)

    xslopes = zeros(xdim, ydim)
    yslopes = zeros(xdim, ydim)

    for i = 2:xdim-1
        for j = 2:ydim-1
            xdiff = heightmap[i+1,j] - heightmap[i,j]
            xslope = xdiff / xstep

            ydiff = heightmap[i,j+1] - heightmap[i,j]
            yslope = ydiff / ystep

            xslopes[i,j] = xslope
            yslopes[i,j] = yslope
        end
    end
    xslopes, yslopes
end

function get_slopes(xslopes, yslopes)
    epsilon::Float64 = 0.00000001
    ux = [1.0, 0.0, 0.0]
    uy = [0.0, 1.0, 0.0]
    uz = [0.0, 0.0, 1.0]
    xdim, ydim = size(xslopes)
    slopes = zeros(size(xslopes))
    for i = 1:xdim
        for j = 1:ydim
            if (abs(xslopes[i,j]) + abs(yslopes[i,j])) > epsilon
                xvec = (uz * xslopes[i,j] + ux)
                yvec = (uz * yslopes[i,j] + uy)
                
                normal = cross(xvec, yvec)
                proj = [normal[1]; normal[2]; 0.0]
                
                normal = normal ./ norm(normal)
                proj = [normal[1], normal[2], 0.0]
                length = norm(proj) / normal[3]

                slopes[i,j] = length
            end
        end
    end
    slopes
end

function make_edge_mask(mask, slopes)
    xdim, ydim = size(mask)
    edge = zeros(Bool, size(mask))

    function isnear(x,y)
        near = false
        near |= x > 1 && slopes[x-1,y] != 0
        near |= x < xdim && slopes[x+1,y] != 0
        near |= y > 1 && slopes[x,y-1] != 0
        near |= y < ydim && slopes[x,y+1] != 0
        near
    end

    for i = 1:xdim
        for j = 1:ydim
            if mask[i,j] && isnear(i,j)
                edge[i,j] = true
            end
        end
    end
    edge
end

function enumerate_nonzero(mask::BitArray{2})
    xdim, ydim = size(mask)
    enumerated = zeros(Int64, size(mask))
    count::Int64 = 0;
    for i = 2:xdim-1
        for j = 2:ydim-1
            if mask[i,j]
                count += 1
                enumerated[i,j] = count
            end
        end
    end
    enumerated, count
end

function make_equation_system(enumerated::Array{Int64,2},
                              edge_mask::Array{Bool,2},
                              count::Int64,
                              dx::Array{Float64,2}, 
                              dy::Array{Float64,2})
    xdim, ydim = size(enumerated)
    xvals = zeros(Int64,0)
    yvals = zeros(Int64,0)
    vals = zeros(Float64,0)
    kvals = zeros(count)

    function central_point_eq(i, j, index)
        push!(yvals, index)
        push!(xvals, index)
        push!(vals, -4.0)
        
        xindeces = [enumerated[i-1, j], enumerated[i+1, j],
                    enumerated[i, j-1], enumerated[i, j+1]]
        
        for xind in xindeces
            if xind != 0
                push!(yvals, index)
                push!(xvals, xind)
                push!(vals, 1.0)
            end
        end
        #should maybe take step size into account here.
        kvals[index] =  -(dx[i-1, j] - dx[i, j] + dy[i, j-1] - dy[i, j]) / 4.0
    end

    function edge_point_eq(i, j, index)
        push!(yvals, index)
        push!(xvals, index)
        push!(vals, 1.0)
        kvals[index] = 0.0
    end

    
    for i = 2:xdim-1
        for j = 2:ydim-1
            index = enumerated[i,j]
            if index == 0
                continue
            end
            if edge_mask[i,j]
                edge_point_eq(i, j, index)
            else
                central_point_eq(i, j, index)
            end            
        end
    end
    
    m = sparse(yvals, xvals, vals, count, count)
    m, kvals
end

function reconstruct_from_equations(results, enumerated)
    hmap = zeros(size(enumerated))
    xdim, ydim = size(enumerated)
    for i = 2:xdim-1
        for j = 2:ydim-1
            if enumerated[i,j] > 1
                hmap[i,j] = results[enumerated[i,j]]
            end
        end
    end
    hmap
end

function solve_relief(slopes::Array{Float64,2},
                      xslopes::Array{Float64,2}, 
                      yslopes::Array{Float64,2},
                      mask::Array{Bool,2})                      
    edge_mask = make_edge_mask( .!mask, slopes)

    #enumerate all elements where the mask is true (let the first element be the null element)
    enumerated, count = enumerate_nonzero(mask .| edge_mask)

    #generate K and V1 and V2 matrices - size = N + 1, {N+1, 4}
    eqmat, kvals = make_equation_system(enumerated, edge_mask, count, xslopes, yslopes)
    
    #solve the system
    results = eqmat \ kvals

    #put the values back in place, and return
    relief = reconstruct_from_equations(results, enumerated)
    relief, mask
end


function reliefify_heightmap(heightmap::Array{Float64,2}, 
                             xrange::Float64, yrange::Float64, 
                             alpha::Float64, theta::Float64)
    #find gradients of the model
    xslopes, yslopes = make_gradients(heightmap, xrange, yrange)
    slopes = get_slopes(xslopes, yslopes)

    slopes_nosil = map((x) -> x < theta ? x : 0.0, slopes)
    compressed_slopes = map((x) -> (1.0 / alpha) * log(1 + alpha*x), slopes_nosil)
    
    compression_factors = map((slopes, compressed) -> slopes > 0.0 ? slopes / compressed : 1.0,  
                              slopes_nosil, compressed_slopes)

    xslopes[slopes_nosil .== 0.0] .= 0.0
    yslopes[slopes_nosil .== 0.0] .= 0.0
    
    xslopes_compressed = xslopes ./ compression_factors
    yslopes_compressed = yslopes ./ compression_factors

    mask = zeros(Bool, size(heightmap))
    mask[heightmap .>= 0.001] .= true

    solve_relief(slopes, xslopes_compressed, yslopes_compressed, mask)
end

