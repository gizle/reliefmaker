function mesh_bounding_box(mesh)
    maxes = [-Inf -Inf -Inf]
    mins = [Inf Inf Inf]
    
    for triangle in mesh
        for i = 1:3
            maxes[i] = max(maxes[i], triangle[1][i], triangle[2][i], triangle[3][i])
            mins[i] = min(mins[i], triangle[1][i], triangle[2][i], triangle[3][i])
        end
    end
    maxes, mins
end

function draw_triangle!(zbuffer, triangle)
    p1 = triangle[1]
    p2 = triangle[2]
    p3 = triangle[3]

    xspan = max(p1[1], p2[1], p3[1]) - min(p1[1], p2[1], p3[1])
    yspan = max(p1[2], p2[2], p3[2]) - min(p1[2], p2[2], p3[2])
    if ceil(max(xspan, yspan)) <= 0
        return
    end
    nsteps = Int(ceil(max(xspan, yspan)*3))
    step = 1.0 / nsteps
    
    for u= 0.0:step:1.0
        for vv = u:step:1.0
            v = 1-vv
            w = 1.0 - u - v
            x = p1[1] * u + p2[1] * v + p3[1] * w
            y = p1[2] * u + p2[2] * v + p3[2] * w
            z = p1[3] * u + p2[3] * v + p3[3] * w
            zbuffer[Int(round(x+1)), Int(round(y+1))] = max(zbuffer[Int(round(x+1)), Int(round(y+1))], z)
        end
    end
end

function flip_bitmap(bitmap)
    newmap = zeros(size(bitmap))
    N, M = size(bitmap)
    for i = 1:M
        newmap[1:N, M-i+1] = bitmap[1:N, i]
    end
    newmap
end

function make_zbuffer(mesh, dims)
  
    zbuffer = zeros(Float32, Int(ceil(dims[1]))+1, Int(ceil(dims[2]))+1)

    for triangle in mesh
        draw_triangle!(zbuffer, triangle)
    end
    zbuffer
end
