
using LinearAlgebra
using GeometryBasics

struct Triangle
    p0::Array{Float64,1}
    p1::Array{Float64,1}
    p2::Array{Float64,1}
    normal::Array{Float64,1}
end

function generate_model(relief::Array{Float64,2}, mask::Array{Bool,2})
    
    function get_triangle(heightmap, p0x, p0y, p1x, p1y, p2x, p2y, direction)
        p0 = [p0x, p0y, heightmap[p0x, p0y]]
        p1 = [p1x, p1y, heightmap[p1x, p1y]]
        p2 = [p2x, p2y, heightmap[p2x, p2y]]
        normal = cross(p1 - p0, p2 - p0)
        normal = normal ./ norm(normal)
        if direction > 0
            Triangle(p0, p1, p2, normal  .* direction)
        else
            Triangle(p2, p1, p0, normal  .* direction)
        end
    end

    function make_mesh(heightmap, mask, direction)
        triangles = []
        for i = 2:size(relief)[1] - 1
            for j = 2:size(relief)[2] - 1
                p0 = mask[i,j]
                p1 = mask[i, j+1]
                p2 = mask[i-1, j+1]
                p3 = mask[i-1, j]
                if p0 && p1 && p2 && p3
                    push!(triangles, get_triangle(heightmap, i, j, i-1, j+1, i-1, j, direction))
                    push!(triangles, get_triangle(heightmap,i, j, i, j+1, i-1, j+1, direction))
                elseif p0 && p2 && p3
                    push!(triangles, get_triangle(heightmap,i, j, i-1, j+1, i-1, j, direction))
                elseif p0 && p1 && p3
                    push!(triangles, get_triangle(heightmap,i, j, i, j+1, i-1, j, direction))
                elseif p0 && p1 && p2
                    push!(triangles, get_triangle(heightmap,i, j, i, j+1, i-1, j+1, direction))
                elseif p1 && p2 && p3
                    push!(triangles, get_triangle(heightmap,i, j+1, i-1, j+1, i-1, j, direction))
                end
            end
        end
        triangles
    end

    function get_upper_side_triangle(upper, lower, i1, j1, i2, j2)
        p3 = [i2, j2, lower[i2, j2]]
        p2 = [i2, j2, upper[i2, j2]]
        p1 = [i1, j1, upper[i1, j1]]
        normal = cross(p2 - p3, p1 - p3)
        normal = normal ./ norm(normal)
        Triangle(p3, p1, p2, normal)
    end

    function get_lower_side_triangle(upper, lower, i1, j1, i2, j2)
        p3 = [i1, j1, lower[i1, j1]]
        p1 = [i2, j2, lower[i2, j2]]
        p0 = [i1, j1, upper[i1, j1]]
        normal = cross(p3 - p0, p1 - p0)
        normal = normal ./ norm(normal)
        Triangle(p0, p1, p3, normal)
    end

    function add_side_triangles(upper_heights, lower_heights, i1, j1, i2, j2)
        return get_upper_side_triangle(upper_heights, lower_heights, i1, j1, i2, j2),
               get_lower_side_triangle(upper_heights, lower_heights, i1, j1, i2, j2)
    end

    function make_sidemesh(mask, heightmap, minplane)
        tris = []
        for i = 2:size(relief)[1]
            for j = 1:size(relief)[2] - 1
                p0 = mask[i,j]
                p1 = mask[i, j+1]
                p2 = mask[i-1, j+1]
                p3 = mask[i-1, j]
                if p0 && p1 && p2 && p3 #0
                    continue
                elseif p0 && p1 && p2 #1
                    t1,t2 = add_side_triangles( heightmap, minplane, i, j, i-1, j+1)
                elseif p0 && p2 && p3 #2
                    t1, t2 = add_side_triangles( heightmap, minplane, i-1, j+1, i, j)
                elseif p0 && p1 && p3 #3
                    t1, t2 = add_side_triangles( heightmap, minplane, i-1, j, i, j+1)
                elseif p1 && p2 && p3 #8
                    t1, t2 = add_side_triangles( heightmap, minplane, i, j+1, i-1, j)
                elseif p0 && p1 #4
                    t1, t2 = add_side_triangles( heightmap, minplane, i, j,  i, j+1 )
                elseif p0 && p3 #6
                    t1, t2 = add_side_triangles( heightmap, minplane, i-1, j, i, j)
                elseif p2 && p3 #9
                    t1, t2 = add_side_triangles( heightmap, minplane, i-1, j+1, i-1, j)
                elseif p1 && p2 #10
                    t1, t2 = add_side_triangles( heightmap, minplane, i, j+1, i-1, j+1)
                else
                    continue
                end
                push!(tris, t1)
                push!(tris, t2)
            end
        end
        tris
    end

    function trim_orphan_nodes(oldmask)
        dim1 = size(oldmask)[1]
        dim2 = size(oldmask)[2]
        newmask = zeros(Bool, dim1, dim2)
        for i = 2:dim1-1
            for j = 2:dim2-1
                p0 = oldmask[ i-1, j-1]
                p1 = oldmask[ i  , j-1]
                p2 = oldmask[ i+1, j-1]
                p3 = oldmask[ i-1, j  ]
                p4 = oldmask[ i  , j  ]
                p5 = oldmask[ i+1, j  ]
                p6 = oldmask[ i-1, j+1]
                p7 = oldmask[ i  , j+1]
                p8 = oldmask[ i+1, j+1]
                if !p4
                    newmask[i,j] = false
                    continue
                end
                if (p0 && p1) || (p1 && p2) || (p2 && p5) || (p5 && p8) || (p8 && p7) || (p7 && p6) || (p6 && p3) || (p3 && p0)
                    newmask[i,j] = true
                else
                    newmask[i,j] = false
                end
            end
        end
        newmask
    end

maxh = maximum(relief)
minh = minimum(relief)

tmask = trim_orphan_nodes(mask)
triangles = make_mesh(relief, tmask, 1.0)

minplane = ones(size(relief)) .* (minh - (maxh - minh) * 0.1)
#minplane = zeros(size(relief))
triangles = vcat(triangles, make_mesh(minplane, tmask, -1.0))
sidemesh = make_sidemesh(tmask, relief, minplane)
triangles = vcat(triangles, sidemesh)

points = Point3f[]
faces = GLTriangleFace[]
normals = Vec3f[]

i=0
for triangle in triangles
    p0 = Point3f(triangle.p0[1], triangle.p0[2],  triangle.p0[3])
    p1 = Point3f(triangle.p1[1], triangle.p1[2],  triangle.p1[3])
    p2 = Point3f(triangle.p2[1], triangle.p2[2],  triangle.p2[3])
    normal = Vec3f(triangle.normal[1], triangle.normal[2], triangle.normal[3])
    push!(points, p0)
    push!(points, p1)
    push!(points, p2)
    push!(normals, normal)
    face = GLTriangleFace(i*3+1, i*3+2, i*3+3)
    i += 1
    push!(faces, face)
end
#Mesh(meta(points; normals=normals), faces)
Mesh(points, faces)
end
